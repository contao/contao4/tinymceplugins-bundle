<?php

namespace Contao;

if ($GLOBALS['TL_CONFIG']['useRTE']):

    $external = '';
    if (isset($GLOBALS['TinyMCE']['external']) && is_array($GLOBALS['TinyMCE']['external'])) {
        $external = [];
        foreach ($GLOBALS['TinyMCE']['external'] as $arr) {
            foreach ($arr as $key => $val) {
                $external[] = sprintf('"%s":"%s"', $key, $val);
            }
        }
        $external = implode(',', $external);
    }

    // Toolbar
    $toolbar = 'link unlink | image | bold italic | styleselect | bullist numlist outdent indent | undo redo | code';
    if (is_array($GLOBALS['TinyMCE']['toolbar'])) {
        $toolbar = [];
        foreach ($GLOBALS['TinyMCE']['toolbar'] as $val) {
            $toolbar[] = sprintf('"%s"', $val);
        }

        $toolbar = implode(',', $toolbar);
    }

    // Menu
    $menubar = 'file edit insert view format table';
    if (is_array($GLOBALS['TinyMCE']['menu'])) {
        $menu    = [];
        $menubar = [];
        foreach ($GLOBALS['TinyMCE']['menu'] as $key => $arr) {
            $menubar[] = $key;
            $var       = [];
            foreach ($arr as $title => $items) {
                $var[] = sprintf('{title : "%s"  , items : "%s"}', $title, $items);
            }
            $var    = implode('', $var);
            $menu[] = sprintf('%s:%s', $key, $var);
        }
        $menu    = implode(',', $menu);
        $menubar = implode(' ', $menubar);
    }

    // Style Formats
    if (is_array($GLOBALS['TinyMCE']['style_formats'])) {
        $style_formats = [];
        foreach ($GLOBALS['TinyMCE']['style_formats'] as $title => $items) {
            $arr = [];
            foreach ($items as $item) {
                $var = [];
                foreach ($item as $key => $val) {
                    $var[] = sprintf('%s : "%s"', $key, $val);
                }
                $var   = implode(',', $var);
                $arr[] = sprintf('{%s}', $var);
            }
            $arr             = implode(',', $arr);
            $style_formats[] = sprintf('{title: "%s", items: [%s]}', $title, $arr);
        }
        $style_formats = implode(',', $style_formats);
    }

    //Content CSS
    $contentCss = [];
    if (isset($GLOBALS['TinyMCE']['bootstrap_css_file'])) {
        $contentCss[] = $GLOBALS['TinyMCE']['bootstrap_css_file'];
    }
    if (isset($GLOBALS['TinyMCE']['importcss_file_filter'])) {
        $contentCss[] = TL_ASSETS_URL.'assets/css/'.$GLOBALS['TinyMCE']['importcss_file_filter'];
    }
    if (isset($GLOBALS['TinyMCE']['content_css'])) {
        $contentCss[] = TL_ASSETS_URL.'assets/css/'.$GLOBALS['TinyMCE']['content_css'];
    }
    $contentCss = implode(',', $contentCss);


    ?>

	<script>window.tinymce || document.write('<script src="<?= $this->asset('js/tinymce.min.js', 'contao-components/tinymce4') ?>">\x3C/script>')</script>
	<script>
        window.tinymce && tinymce.init({
            selector: '#<?= $this->selector ?>',
            min_height: 336,
            language: '<?= Backend::getTinyMceLanguage() ?>',
            element_format: 'html',
            document_base_url: '<?= Environment::get('base') ?>',
            entities: '160,nbsp,60,lt,62,gt,173,shy',
            branding: false,
            setup: function (editor) {
                editor.getElement().removeAttribute('required');
                document.querySelectorAll('[accesskey]').forEach(function (el) {
                    editor.addShortcut('access+' + el.accessKey, el.id, function () {
                        el.click();
                    });
                });
            },
            init_instance_callback: function (editor) {
                if (document.activeElement && document.activeElement.id && document.activeElement.id == editor.id) {
                    editor.editorManager.get(editor.id).focus();
                }
                editor.on('focus', function () {
                    Backend.getScrollOffset();
                });
            },

            <?php $this->block('picker'); ?>
            file_picker_callback: function (callback, value, meta) {
                Backend.openModalSelector({
                    'id': 'tl_listing',
                    'title': document.getElement('.tox-dialog__title').get('text'),
                    'url': Contao.routes.backend_picker + '?context=' + (meta.filetype == 'file' ? 'link' : 'file') + '&amp;extras[fieldType]=radio&amp;extras[filesOnly]=true&amp;extras[source]=<?= $this->source ?>&amp;value=' + value + '&amp;popup=1',
                    'callback': function (table, val) {
                        callback(val.join(','));
                    }
                });
            },
            file_picker_types: <?= json_encode($this->fileBrowserTypes) ?>,
            <?php $this->endblock(); ?>

            <?php $this->block('content_css'); ?>
            content_css: '<?= $contentCss ?>',
            <?php if (isset($style_formats)): ?>
            style_formats_merge: false,
            style_formats: [<?= $style_formats?>],
            <?php endif; ?>
            <?php $this->endblock(); ?>

            <?php $this->block('plugins'); ?>
            plugins: 'autosave charmap code fullscreen image importcss link lists paste searchreplace stripnbsp tabfocus table visualblocks visualchars',
            <?php $this->endblock(); ?>

            ///external_plugins: {<?= $external ?>},

            <?php $this->block('valid_elements'); ?>
            extended_valid_elements: 'q[cite|class|title],article,section,hgroup,figure,figcaption',
            <?php $this->endblock(); ?>

            <?php $this->block('menubar'); ?>
            menubar: '<?= $menubar ?>',
            <?php if (isset($menu)): ?>
            menu: {<?= $menu ?>},
            <?php endif; ?>
            <?php $this->endblock(); ?>

            <?php $this->block('toolbar'); ?>
            toolbar: [<?= $toolbar ?>],
            <?php $this->endblock(); ?>

            <?php $this->block('contextmenu'); ?>
            contextmenu: false,
            <?php $this->endblock(); ?>

            <?php $this->block('cache_suffix'); ?>
            cache_suffix: '?v=<?= $this->assetVersion('js/tinymce.min.js', 'contao-components/tinymce4') ?>',
            <?php $this->endblock(); ?>

            <?php $this->block('custom'); ?>
            <?php if (isset($GLOBALS['TinyMCE']['bootstrap_css_file'])): ?>
            importcss_file_filter: "<?= $GLOBALS['TinyMCE']['bootstrap_css_file'] ?>",
            importcss_groups: [
                {title: "Bootstrap Display", filter: /^(\.display-?)/},
                {title: "Bootstrap Text", filter: /^(\.lead|\.teaser|\.mark|\.initialism|\.text-.*)/},
                {title: "Bootstrap Background", filter: /^(\.bg-.*)/},
                {title: "Bootstrap Border", filter: /^(\.border.*)/},
            ],
            importcss_selector_filter: /^^(\.lead|\.teaser|\.mark|\.initialism|\.text-.*|\.bg-.*|\.border.*|\.display-?)/,
            <?php elseif (isset($GLOBALS['TinyMCE']['importcss_file_filter'])) : ?>
            importcss_file_filter: "<?= sprintf("@import url('%s/assets/css/%s');", TL_ASSETS_URL, $GLOBALS['TinyMCE']['importcss_file_filter']); ?>",
            importcss_groups: [{title: "<?= $GLOBALS['TL_LANG']['MSC']['TinyMCE_ThemesFormats'] ?>"}],
            <?php endif; ?>
            <?php $this->endblock(); ?>

            browser_spellcheck: true,
            tabfocus_elements: ':prev,:next',
            importcss_append: true,
            paste_as_text: true
        });
	</script>
<?php endif; ?>
<style>
    .tox .tox-collection__item-label {line-height: 1}
</style>