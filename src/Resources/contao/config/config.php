<?php
/**
 * Contao 4 TinyMCE Plugins Bundle
 *
 * @copyright 2011, 2014, 2018 agentur fipps e.K.
 * @author    Arne Borchert
 * @package   fipps\contao-tinymceplugins-bundle
 * @license   LGPL 3.0+
 */

$GLOBALS['TL_HOOKS']['parseBackendTemplate'][] = ['Fipps\TinymcepluginsBundle\Hooks', 'parseBackendTemplate'];

$GLOBALS['TinyMCE']['extended_valid_elements'] = 'small, mark, address, footer';
$GLOBALS['TinyMCE']['importcss_file_filter']   = 'TinyMCE.css';
$GLOBALS['TinyMCE']['content_css']             = 'Styles.css';

//$GLOBALS['TinyMCE']['menu']['file']   = [
//    'File' => 'newdocument',
//];
$GLOBALS['TinyMCE']['menu']['edit']   = [
    'Edit' => 'undo redo | cut copy paste pastetext | selectall | searchreplace',
];
$GLOBALS['TinyMCE']['menu']['insert'] = [
    'Insert' => 'link | charmap | hr',
];
$GLOBALS['TinyMCE']['menu']['view']   = [
    'View' => 'visualaid visualblocks',
];
$GLOBALS['TinyMCE']['menu']['format'] = [
    'Format' => 'bold italic underline strikethrough superscript subscript | formats | removeformat',
];
$GLOBALS['TinyMCE']['menu']['tools']  = [
    'Tools' => 'spellchecker code',
];

$GLOBALS['TinyMCE']['toolbar'] = [
    'spellchecker | cut copy paste pastetext | searchreplace | undo redo | link unlink | charmap',
    'styleselect | bold italic strikethrough superscript subscript | alignleft aligncenter alignright | bullist numlist outdent indent | removeformat',
];

$GLOBALS['TinyMCE']['style_formats']['Headers'] = [
    [
        'title'  => 'Heading 1',
        'format' => 'h1',
    ],
    [
        'title'  => 'Heading 2',
        'format' => 'h2',
    ],
    [
        'title'  => 'Heading 3',
        'format' => 'h3',
    ],
    [
        'title'  => 'Heading 4',
        'format' => 'h4',
    ],
    [
        'title'  => 'Heading 5',
        'format' => 'h5',
    ],
    [
        'title'  => 'Heading 6',
        'format' => 'h6',
    ],
];
$GLOBALS['TinyMCE']['style_formats']['Blocks']  = [
    [
        'title'  => 'Paragraph',
        'format' => 'p',
    ],
    [
        'title'  => 'Blockquote',
        'format' => 'blockquote',
    ],
];
$GLOBALS['TinyMCE']['style_formats']['Inline']  = [
    [
        'title'  => 'small',
        'inline' => 'small',
    ],
    [
        'title'  => 'mark',
        'inline' => 'mark',
    ],
];

